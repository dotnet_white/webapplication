using Microsoft.EntityFrameworkCore;
using OdeToFood.core;
// command to execute in shell to get info of dbcontext
//dotnet ef dbcontext info -s ../WebApplication/WebApplication.csproj 
// Creation of migrations
// dotnet ef migrations add initialcreate -s ../WebApplication/WebApplication.csproj
// Update Database
// dotnet ef database update -s ../WebApplication/WebApplication.csproj
namespace OdeToFood.Data
{
    public class OdeToFoodDbContext :DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }

        public OdeToFoodDbContext(DbContextOptions<OdeToFoodDbContext> options) 
            :base(options)
        {
            
        }
    }
}