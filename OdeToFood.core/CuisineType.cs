namespace OdeToFood.core
{
    public enum CuisineType
    {
        none,
        Mexican,
        Italian,
        Indian
    }
}