using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OdeToFood.core;
using OdeToFood.Data;

namespace WebApplication.Pages.Restaurants
{
    public class Details : PageModel
    {
        private readonly IRestaurantData _restaurantData;
        public Restaurant restaurant { get; set; }
        [TempData]
        public string Message { get; set; }

        public Details(IRestaurantData restaurantData)
        {
            _restaurantData = restaurantData;
        }
        
        public IActionResult OnGet(int restaurantId)
        {
            restaurant = _restaurantData.GetById(restaurantId);
            if (restaurant == null)
            {
                return RedirectToPage("./NotFound");
            }

            return Page();
        }
    }
}